// Navbar
document.querySelector(".navbar__menu__button").addEventListener("click", function(){
  document.querySelectorAll(".navbar__menu").forEach(function(nav) {
    nav.classList.toggle("navbar__menu--open");
  })
});

// Youtube-embed
document.addEventListener("DOMContentLoaded", function() {
  var vidIterator, video;
  var vidList = document.getElementsByClassName("youtube");
  for (vidIterator = 0; vidIterator < vidList.length; vidIterator++) {
    video = document.createElement("div");
    video.setAttribute("data-id", vidList[vidIterator].dataset.id);
    video.innerHTML = '<img class="youtube__thumb" src="https://i.ytimg.com/vi/'+
      vidList[vidIterator].dataset.id+'/hqdefault.jpg"/>'+
      '<div class="youtube__play">PLAY</div>';
    video.onclick = makeIframe;
    vidList[vidIterator].appendChild(video);
  }
});
function makeIframe() {
  var iframe = document.createElement("iframe");
  iframe.setAttribute(
    "src",
    "https://www.youtube.com/embed/"+this.dataset.id+"?autoplay=1"
  );
  iframe.setAttribute("class", "youtube__iframe");
  iframe.setAttribute("frameborder", "0");
  iframe.setAttribute("allowfullscreen", "1");
  this.parentNode.replaceChild(iframe, this);
}
