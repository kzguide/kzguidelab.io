* [ ] Shortcode Stub article
* [ ] Style background with pattern? 
* [x] Check RelLinks to not target=_blank
* [x] Tables should scroll-overflow
* [x] Add language selector
* [x] Style Tables
* [x] Style code snippets
* [x] Style Inline code
* [x] Style blockquote
* [x] Style Toggle sections
* [x] Style Links
* [x] Shortcode Note
* [x] Shortcode "Credits/Thanks to/Epigraph"
* [x] Youtube video replaced by image?
* [x] Last update text in each article
* [x] Scroll up button?
* [x] Toggle sections
* [x] About page
* [x] Home page
* [x] 404 page
* [x] Anchor to headers
* [x] List pages
* [x] Breadcumb at article's start.
* [x] Add a footer
* [x] Boxed articles

---
* [-] Purge style.scss [Only mobile's nav is left]
* [x] Make multilingual
* [x] Gzip all
* [x] Minify deps
* [x] Purge Dependencies
* [x] Fonts
* [x] CSS fallback fonts when loading
