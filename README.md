## Build locally
* Fork, clone or download project.
* Install [hugo-extended](https://github.com/gohugoio/hugo/releases).
* Preview project: `hugo server -D`. 
* (Optional) Generate static site: `hugo`.

## Preview site
After run `hugo server -D`, site can be accessed under [localhost:1313](localhost:1313)

## Directory structure
Pending...

## Templating
Pending...
