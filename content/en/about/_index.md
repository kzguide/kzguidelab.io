---
title: "About"
description: ""
weight: 5
date: 2019-02-01T12:00:00-03:00
displayInMenu: true
---

## About
---
KZ-Guide is a simple, fast and static site hosted at 
[Gitlab](https://gitlab.com)'s pages.\

The project is open source so you can check the entire 
[repository](https://gitlab.com/kzguide/kzguide.gitlab.io) whenever you want.
Feel free to submit a PR, clone or fork it.

If you want to make a suggestion, point an error or provide any feedback use the 
[contact form](#contact) available below.

\
\
## Thanks
---
### People
* FAME
* Johnnyy
* Flibo
* dEMolite

### Communities

* [Xtreme-Jumps](https://www.xtreme-jumps.eu)
* [KZ-Rush](https://www.kz-rush.ru)
* [Cosy-Climbing](https://www.cosy-climbing.net)

\
\
## Contact
---
<form class="contact" action="https://formspree.io/kzguide@yandex.com" method="POST">
  <label class="contact__label" for="name">Name</label>
  <input class="contact__input" type="text" name="name">
  <label class="contact__label" for="replyto">Mail</label>
  <input class="contact__input" type="email" name="replyto">
  <label class="contact__label" for="replyto">Message</label>
  <textarea class="contact__text" name="message"></textarea>
  <button class="contact__button" class="button" type="submit">Send!</button>
</form>
