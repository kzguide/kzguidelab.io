---
title: "Flibobug"
description: ""
group: "Mechanics"
weight: 3
date: 2019-02-01T12:00:00-03:00
---

## Flibobug
---
Flibobug or _collision model bug_, is a consequence of model's boundaries being 
grid aligned.

As you may know, the hull of the player in counter-strike is a rectangular 
parallelepiped with a base of 32x32 units and, if standing, 72 units
of height.

{{<figure src="/images/collision-model.png" alt="collision-model" caption="Collision Model">}}

Try to imagine a top-down perspective of the game's grid where we can represent
the player by its 32x32 units square base. 

{{<figure src="/images/flibobug-grid.png" alt="flibobug-grid" caption="Top-down perspective">}}

As mentioned, model is grid aligned so it won't be allowed to rotate in the x/y 
plane causing every move to be done with a different collision model length 
based on the offset angle between movement's direction and grid.\
This length will be minimal when direction is parallel to grid's vertical or 
horizontal axis (0° / 90° / 180° / 270°) and maximal when movement's direction 
is in the middle of both (45° / 135° / 225° / 315°).

{{<figure src="/images/flibobug-square.png" alt="flibobug-square" caption="Player's model measured">}}

After all the explanations, we can infer that when jumping diagonally
we'll need to actually travel less distance in order to reach the other end
because we will be covering the difference with our own collision model.

\
_Example:_

{{<figure src="/images/flibobug-example.png" alt="Flibobug example" caption="Flibobug example">}}

On the left side of the previous diagram, there's a vertical aligned jump of 250
units. With a model of 32 units, you'll need to jump 218 units (as long as you 
jump at the edge of the block).
{{<highlight c>}}
250 - 32 = 218
{{</highlight>}}

On the right side, instead, there's a 45° diagonal jump of 250 units as well.
This time, with a model of 32 units, you'll need to jump ≈205 units.\
The diagonal length of a square is `sqrt(2) * side`
{{<highlight c>}}
250 - sqrt(2) * 32 ≈ 204.74
{{</highlight>}}

The difference in distance is just that of the difference in model's lengths:
{{<highlight c>}}
sqrt(2) * 32 - 32 ≈ 13.25
{{</highlight>}}

