---
title: "Sliding"
description: ""
group: "General"
weight: 3
date: 2019-02-01T12:00:00-03:00
---

## Slide
---
Sliding is performed on slanted wall's blocks.

{{%note%}}
A high FPS rate is needed to slide. If not, it'll become unstable.
{{%/note%}}

To climb this kind of blocks

* Look towards the slope.
* Press and hold {{<kbd W>}} to _slide up_ the block's wall.
* At last, try not to change view angle, it'll make you slide to one side or another.

\
To _slide_ this kind of blocks

* Jump to the block.
* Align your view to look forward, parallel to the slope's block.
* Press and hold {{<kbd A>}} or {{<kbd D>}} based on which side the block is.
* Adjust your view: away to _slide down_ the slope or inside to _slide up_ of it.

\
{{<yt id="iC7qT1eN78M">}}
{{<epigraph title="Credits " text="xtreme-jumps.eu" link="https://xtreme-jumps.eu" yt="1">}}
