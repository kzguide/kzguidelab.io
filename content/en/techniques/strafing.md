---
title: "Strafing"
description: ""
group: "General"
weight: 2
date: 2019-02-01T12:00:00-03:00
---

## Pre strafe
---
Prestrafing is the act of gaining speed on ground before actually performing the jump.

* Start running holding {{<kbd W>}}.
* While keeping {{<kbd W>}} pressed, hold either {{<kbd A>}} or {{<kbd D>}} as desired.
* At the same time, start doing a smooth curve with the mouse in the same direction.
* At last, when ready to jump, release {{<kbd W>}} and jump off while still pressing the strafe key.

{{<yt id="DLtj2EiZf84">}}
{{<epigraph title="Credits " text="xtreme-jumps.eu" link="https://xtreme-jumps.eu" yt="1">}}


## Air strafes
---
Airstrafes let you accelerate while in air, they are one of the most important movements you should learn.

* Jump off.
* Press and hold a strafe key, {{<kbd A>}} to the left or {{<kbd D>}} to the right.
* Move your mouse horizontally in the same direction as the chosen strafe key.

{{%note%}}
You could alternate airstrafes mid-air as much as you want.\
You'll need to change both strafe key and mouse movement to the opposite direction.
{{%/note%}}

{{<yt id="T-rSygUEgag">}}
{{<epigraph title="Credits " text="xtreme-jumps.eu" link="https://xtreme-jumps.eu" yt="1">}}
