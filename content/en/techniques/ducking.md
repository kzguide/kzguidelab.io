---
title: "Ducking"
description: ""
group: "General"
weight: 4
date: 2019-02-01T12:00:00-03:00
---

## Duck
---
Ducking or crouching not only reduces the height of player in a half but it 
also cuts its horizontal speed by 2/3 (you move 3 times slower than standing).

### Duck in ground
* Player stands with 72 units height model.
* Player presses and holds duck key.
* Game waits 0.4secs and after that crouches model to 36 units height.

Contrary to what is perceived, player only gets in duck state after 0.4 seconds 
of key being held. During that time engine _only_ lowers view-camera giving
the false impression of crouching.\
Unducking only takes 1 frame from the moment duck key is released.\
The actual unduck process on ground involves elevating the player's model 
center by 18 units and then increasing the top and bottom boundaries by
18 units each.

{{<figure src="/images/duck-in-ground.png" alt="Duck in ground" caption="Duck in ground diagram">}}

### Duck in air
* Player's in air with 72 units height model.
* Player presses and holds duck key.
* Game automatically changes model's height to 36 units.

The first and obvious difference of ducking in air is that it happens without 
delay, which means the action is performed on the next frame the key was pressed.
The other difference is that instead of moving the player's centre, only top and 
bottom boundaries of model are reduced by 18 units while the center of it 
remains at the same place.\
Same as on ground, unducking takes 1 frame from the moment duck key is released
but player's center doesn't change position.

{{<figure src="/images/duck-in-air.png" alt="Duck in air" caption="Duck in air diagram">}}

## Stand-up
---
When unducking, engine preventively traces if model's going to get stuck
because, for example, a ceiling above the head.\
Unducking in air implies not only increasing model's top boundary but also 
bottom one, meaning we can got stuck on the floor as well.

Stand-ups occur when you try to unduck in air close to the floor.
As stated, engine won't allow you to unduck there, right before touching the 
ground, because model's would end up stuck. So, even if duck key is released, 
engine will keep you in a ducked position till the moment you actually 
touch the ground (because only there it'll be able to unduck you properly, 
elevating the center and increasing boundaries of model).
 
{{<figure src="/images/stand-up.png" alt="Stand-up" caption="Stand-up diagram">}}

## Duck Tap
---
{{%note%}}
It's recommended to bind the mouse wheel (either up or down) to duck tap.\
`bind mwheelup +duck`\
`bind mwheeldown +duck`\
This way you are going to send '`+duck;-duck`' with each wheel tick.
{{%/note%}}

When on ground, engine takes 0.4secs to actually duck. But if key is released 
before that time passes then something peculiar happens, the engine 'thinks' we 
are in duck (even if our model's not) and proceeds to unduck anyway.

To conclude, the result of 'unducking' the player when still standing is a 
mini-jump of 18 units. One which is not affected by stamina like normal jumps do 
but nevertheless put us up in the air like one.

{{<figure src="/images/duck-tap.png" alt="Duck tap" caption="Duck Tap diagram">}}

## Double Duck
---
If you duck after doing a duck-tap then you end ≈36 units over the ground 
(18 units from the duck-tap and another 18 units from ducking mid air).\

This technique shines in two situations:\
_1._ Climbing stairs with steps of 35 units or less height:\
Compared to jumping over every step, while doubleducking you spend less time 
in air and avoid triggering the stamina's associated slowdown.\

_2._ To prevent slowdowns at the end of jumps:\
Instead of running after landing you can perform a doubleduck right away and 
cover some distance in air, decreasing *fuser2*'s stamina variable and 
avoiding losing speed due to friction in the meantime.

{{<figure src="/images/double-duck.png" alt="Double Duck" caption="Double Duck diagram">}}

{{<yt id="qMZzdHGGfxQ">}}
{{<epigraph title="Credits " text="xtreme-jumps.eu" link="https://xtreme-jumps.eu" yt="1">}}
