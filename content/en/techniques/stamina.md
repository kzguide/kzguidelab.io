---
title: "Stamina"
description: ""
group: "Mechanics"
weight: 2
date: 2019-02-01T12:00:00-03:00
---

## Stamina
---
What we vulgarly know as _stamina_ in Counter-Strike it's actually dictated by 
a variable in code named `fuser2`. It wasn't present at early versions of the 
game but eventually was added during the updates it received.

`fuser2` starts at a value of 0 when player spawns and every time a jump is 
made it resets to 1315.789429.
{{<highlight c>}}
void PM_Jump() {
  ...
  pmove->fuser2 = 1315.789429;
  ...
}
{{</highlight>}}

Once set during jumping, `fuser2` diminishes every frame by frame's length 
(in milliseconds). Therefore, with a rate of 100fps it will be reduced each 
frame by 10 taking ≈1.31sec to reach 0 again and stop affecting our movements.
{{<highlight c>}}
void PM_ReduceTimers() {
  ...
  if (pmove->fuser2 > 0.0) {
    pmove->fuser2 -= pmove->cmd.msec;
    if (pmove->fuser2 < 0.0) pmove->fuser2 = 0;
  }
  ...
}
{{</highlight>}}


The variable itself alters all three components of the `velocity` vector, 
that's to say vertical and horizontal speeds alike, because it is used by the 
engine inside `PM_WalkMove()` and `PM_Jump()` functions.

**`PM_WalkMove()`**:\
This function is called every time player moves on ground.\
Provided that `fuser2` is greater than 0, its value will be utilized to 
cramp player's horizontal speed (`velocity.x` and `velocity.y`) multiplying 
it by a factor calculated using the following formula:
{{<highlight c>}}
factor = (100 - pmove->fuser2 * 0.001 * 19) * 0.01;
{{</highlight>}}

The shorter the jump the lesser the variable will decay which means a bigger 
slowdown when you start moving on ground after landing. That's why you end up 
walking slower after every jump and why it's more notorious when the jump is up 
to a platform instead of down to it.


**`PM_Jump()`**:\
As its name suggests, this function is called every time player jumps. Here, 
`fuser2` is used to cramp player's vertical speed (`velocity.z`) instead.
The factor is given by the same formula presented before:
{{<highlight c>}}
factor = (100.0 - pmove->fuser2 * 0.001 * 19.0) * 0.01;
{{</highlight>}}

It's because of `fuser2` use in `PM_Jump()` that subsequent bhops have less 
height than the first one. Take into account that when jumping at the same 
ground level we spend 0.66 seconds in air which does not suffice to null 
`fuser2` before touching the ground again (remember we need at least 1.31 
seconds to do so).\

Also, you can now understand why spending more time in air when performing a 
stand-up bhop lessens variable's value letting us gain a little more height in 
the next jump.
