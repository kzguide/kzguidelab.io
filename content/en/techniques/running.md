---
title: "Running"
description: ""
group: "General"
weight: 1
date: 2019-02-01T12:00:00-03:00
---

## Normal run
---
Up to 250{{<fraction u s>}}

* Push and hold {{<kbd W>}} to run.

## Fast run
---
Up to 260{{<fraction u s>}}

* Push and hold {{<kbd W>}} to start running.
* Look sideways with an angle of 45°.
* Start tapping {{<kbd A>}} or {{<kbd D>}} as suited, based on looking direction.

\
Up to ???{{<fraction u s>}}

* Push and hold {{<kbd W>}} to start running.
* Start doing little [prestrafes](/techniques/strafing/#pre-strafe) alternating sides.
* Try to keep run direction as straight as possible.


## Wall rub
---
Up to 276{{<fraction u s>}}

* Start running with {{<kbd W>}} alongside a wall.
* Look sideways with a 45° angle, either away or inside from the wall.
* Press and hold {{<kbd A>}} or {{<kbd D>}} as suited based on wall's side.

\
Up to 277{{<fraction u s>}}

* Start running with {{<kbd W>}} alongside a wall.
* Look inside the wall with a 45° angle.

\
\

---
{{<yt id="mCALtW56bsk" credits="xtreme-jumps.eu" link="">}}
{{<epigraph title="Credits " text="xtreme-jumps.eu" link="https://xtreme-jumps.eu" yt="1">}}
