---
title: "Home"
description: ""
weight: 1
date: 2019-02-01T12:00:00-03:00
displayInMenu: true
---

### 
**KZ Guide** is an attempt to preserve Counter Strike 1.6 Kreedz's climbing mode.\

I think the communities are doing an excellent job in keeping the scene 
competitive, fresh and alive through records, server hosting, forums and others.
They have the infrastructure, the staff and the will to do so.
The only thing that remains unpolished is related to tutorials, guides,
explanations, and alike which are scarce, scattered or outdated.\

Therefore, the main objective of this site is to present 
any relevant gameplay information in a clear and concise way, whereas difficult 
and more technical details will be, to the extent possible, available in 
_physics_' section under each article.

I hope this will encourage people to submit data in order to consolidate a 
wider, clearer and accurate documentation of kz's world.
