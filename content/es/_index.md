---
title: "Inicio"
description: ""
weight: 1
date: 2019-02-01T12:00:00-03:00
displayInMenu: true
---

### 
**KZ Guide** es un intento por preservar el modo KreedZ de Counter Strike 1.6.\

Creo que las comunidades están realizando un excelente trabajo por mantener la
escena competitiva, fresca y activa a través de los sistemas de records, el 
hosteo de servidores, los foros y demás. Poseen la infraestructura, el personal 
y la voluntad para hacerlo. Lo único que permanece aún desdibujado son los 
tutoriales, guías, explicaciones y similares que son escasos, están dispersos y/o 
desactualizados.\

Es por eso que el principal objetivo del sitio es presentar cualquier información 
relevamente al modo de juego de manera clara y concisa. Dejando los detalles 
técnicos y difíciles, en la medida que sea posible, en su respectivo apartado de
_físicas_ dentro de cada artículo.

Espero que esto aliente a las personas a colaborar para poder consolidar una
documentación de kz más amplia, clara y precisa.
